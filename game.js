BlockType = function(mesh, run_distance) {
	this.mesh = mesh
	this.run_distance = run_distance
	this.instantiate = function() {
		return new BlockInstance(this)
	}
}

BlockInstance = function(blocktype) {
	this.mesh = blocktype.mesh.clone()
	this.run_distance = blocktype.run_distance
}

function applyTransform(object) {
	object.updateMatrix();

  object.geometry.applyMatrix( object.matrix );

  object.position.set( 0, 0, 0 );
  object.rotation.set( 0, 0, 0 );
  object.scale.set( 1, 1, 1 );
  object.updateMatrix();
}

var straight = new THREE.Mesh(new THREE.BoxBufferGeometry( 200, 200, 500 ), new THREE.MeshBasicMaterial( {color: 0x0000FF} ) );
straight.position.z = 250;
applyTransform(straight);

var BlockTypes = new Map([
  	[new BlockType(straight, 500), 1.0],
])

function getRandomBlockType() {
	  var normalizer = 0
		var ranges = []
		var blocks = []
		BlockTypes.forEach(function(value, key, _) {
			  blocks.push(key)
				normalizer += value
				ranges.push(normalizer)
		})

		for (var i = 0; i < ranges.length; i++) {
			ranges[i] = ranges[i] / normalizer;
		}

		rnd = Math.random()

		for (var i = 0; i < ranges.length; i++) {
			if (i <= ranges[i]) {
				return blocks[i];
			}
		}
}

function Game(useASCII, showStats, blockCount) {
	// Options
	this.useASCII = useASCII !== undefined ? useASCII : true;
	this.showStats = showStats !== undefined ? showStats : true;
	this.blockCount = blockCount !== undefined ? blockCount : 10;

	// Let's get started
  var width = window.innerWidth || 2;
  var height = window.innerHeight || 2;

  var container = document.createElement( 'div' );
  document.body.appendChild( container );

  var info = document.createElement( 'div' );
  info.style.position = 'absolute';
  info.style.top = '10px';
  info.style.width = '100%';
  info.style.textAlign = 'center';
  container.appendChild( info );

  this.renderer = new THREE.WebGLRenderer();
	this.renderer.setSize(width, height);

	if (this.useASCII) {
		this.effect = new THREE.AsciiEffect(this.renderer, undefined, {resolution: 0.2});
		this.effect.setSize(width, height);
	}
	container.appendChild(((this.useASCII) ? this.effect : this.renderer).domElement);

	this.camera = new THREE.PerspectiveCamera( 70, width / height, 1, 1000 );
	this.camera.position.y = 0;
	this.camera.position.z = 500;

	this.scene = new THREE.Scene();
	this.scene.background = new THREE.Color( 0xf0f0f0 );

	var light = new THREE.PointLight( 0xffffff );
	light.position.set( 500, 500, 500 );
	this.scene.add( light );
	var light = new THREE.PointLight( 0xffffff, 0.25 );
	light.position.set( - 500, - 500, - 500 );
	this.scene.add( light );

	this.lastRenderTime = 0;

	if (this.showStats) {
		stats = new Stats();
		container.appendChild(stats.dom);
	}

	this.init_objects = function() {
		var spg = new THREE.SphereGeometry( 200, 20, 10 );
		spg.computeFlatVertexNormals();
		sphere = new THREE.Mesh( spg, new THREE.MeshLambertMaterial() );
		this.scene.add( sphere );

		// Initialize blocks.
		this.blocks = [getRandomBlockType().instantiate()]
		for (var i = 0; i < this.blockCount; i++) {
			var rndBlock = getRandomBlockType().instantiate()
			rndBlock.mesh.position.y = this.blocks[0].mesh.position.y + this.blocks[0].run_distance
			this.blocks.push(rndBlock)
			this.scene.add(rndBlock.mesh)
		}
	}

	this.start = function() {
		this.startTime = Date.now()
		this.lastRenderTime = this.startTime
		this.render()
	}

	this.render = function() {
		requestAnimationFrame( this.render.bind(this) );

		stats.begin();

		var currentTime = Date.now()
		var dt = currentTime - this.lastRenderTime;
		var totalTime = this.lastRenderTime - this.startTime;
		this.lastRenderTime = currentTime;

		this.update(dt, totalTime);

		//controls.update();
		((this.useASCII) ? this.effect : this.renderer).render( this.scene, this.camera );

		stats.end();
	}

	this.velocity = 0.5 // 10 units per second

	this.update = function(dt, total) {
		//sphere.position.y = Math.abs( Math.sin( total * 0.00002 ) ) * 50;
		sphere.rotation.x = -total * 0.0003;
		sphere.rotation.z = total * 0.0002;

		sphere.position.y += dt * this.velocity
		// Move the camera with the player.
		this.camera.position.y += dt * this.velocity
	}

	this.onWindowResize = function functionName() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();

		this.renderer.setSize( window.innerWidth, window.innerHeight );
		if (this.effect !== undefined) {
		  this.effect.setSize( window.innerWidth, window.innerHeight );
		}
	}

	window.addEventListener('resize', this.onWindowResize.bind(this), false);

	this.init_objects();
}

var game = new Game(false);
game.start();
